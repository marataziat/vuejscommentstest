import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    comments: [{id: 1, text: 'Сообщение 1'}, {id: 2, text: 'Сообщение 2', childOf: 1}, {id: 55, text: 'Сообщение 2', childOf: 1}, {id:3, text: 'Сообщение 3'}, {id: 4, text: 'Сообщение 4', childOf: 1}, {id: 5, text: 'Сообщение 5', childOf: 1}]
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
